jQuery(function($) {
    if( $('#gform_wrapper_1').length ){
        var select = $( "#input_1_7" );
        var slider = $( "<div id='slider'></div>" ).insertAfter( select ).slider({
            min: 1,
            max: 11,
            range: "min",
            step: .0001,
            animate: "slow",
            value: select[ 0 ].selectedIndex + 1,
            slide: function( event, ui ) {
                select[ 0 ].selectedIndex = ui.value - 1;
            }
        });
        $( "#input_3_2" ).on( "change", function() {
            slider.slider( "value", this.selectedIndex + 1 );
        });
        $('#input_1_3').on( "change", function() {
            var profit1 = jQuery('#input_1_3').attr('value');
            var profit2 = Math.trunc(profit1);
            slider.slider( "value", profit2+.5 );
            jQuery("#input_1_7").val(profit2);
        });
    }
} );